package com.example.newmykadai;

public class ProductClass {
    private String image;
    private String name;

    public ProductClass(String image, String name) {
        this.image = image;
        this.name = name;
       // this.rate = rate;
        //this.card = card;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   /* public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }*/
}
