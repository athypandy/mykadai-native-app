package com.example.newmykadai;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.sql.SQLException;


public class MainActivity extends AppCompatActivity {
    CardView grocery,hotel, ayurveda,medicine,ice,bakeries, restaurants, stationary;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Uri uri = getIntent().getData();
        if (uri != null) {
            String path=uri.toString();
           
        }
        grocery=findViewById(R.id.grocery);
        hotel=findViewById(R.id.hotel);
        ayurveda=findViewById(R.id.aurvedha);
        medicine=findViewById(R.id.pharmacy);
        ice=findViewById(R.id.ice);
        bakeries=findViewById(R.id.backery);
        restaurants=findViewById(R.id.restaurent);
        stationary=findViewById(R.id.stationary);

        grocery.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
            Intent intent=new Intent(MainActivity.this,Grocery.class);
            startActivity(intent);


            }
             });

        ayurveda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        restaurants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        stationary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        bakeries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}















