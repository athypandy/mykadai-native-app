package com.example.newmykadai;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.Toast;

import com.mysql.jdbc.ResultSetImpl;
import com.mysql.jdbc.ResultSetMetaData;
import com.mysql.jdbc.StatementImpl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class ProductView extends AppCompatActivity {
    List <ProductClass> product;
    RecyclerView recyclerView;
    private boolean success = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerProduct);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        product = new ArrayList <ProductClass>();
        ConnectData connectData=new ConnectData();
        connectData.execute();

    }

    private class ConnectData extends AsyncTask <String, Void, String> {
        String res = "";
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(ProductView.this, "Synchronising", "Product Loading! Please Wait...", true);

        }

        @Override
        protected String doInBackground(String... params) {

            try {
                ConnectionDB DB = new ConnectionDB();
                DB.connection();
                if (DB.connection() == null) {
                    success = false;
                } else {
                    StatementImpl st = (StatementImpl) DB.connection().createStatement();
                    ResultSetImpl rs = (ResultSetImpl) st.executeQuery("select * from  Products where categorycode='3' and ActiveYn='Yes' and DeleteYn='No'");
                    ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
                    if (rs != null) {
                        while (rs.next()) {
                            try {
                                product.add(new ProductClass(rs.getString("productimage"),rs.getString("ProductName")));
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        res = "Found";
                        success = true;
                    } else {
                        res = "No Data Found";
                        success = false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Writer writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                res = writer.toString();
                success = false;
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            Toast.makeText(ProductView.this, res + "", Toast.LENGTH_SHORT).show();
            if (success == false) {

            } else {
                try {
                    GridLayoutManager gridLayoutManager=new GridLayoutManager(ProductView.this,2,GridLayoutManager.VERTICAL,false);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    ProductAdapter adapter = new ProductAdapter(ProductView.this,product);
                    recyclerView.setAdapter(adapter);
                } catch (Exception ex) {


                }
            }

        }
    }
}