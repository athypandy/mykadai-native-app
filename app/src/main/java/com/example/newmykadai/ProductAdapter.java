package com.example.newmykadai;

import android.content.Context;
import android.graphics.Movie;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mysql.jdbc.ResultSetImpl;
import com.mysql.jdbc.ResultSetMetaData;
import com.mysql.jdbc.Statement;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

@SuppressWarnings("ALL")
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private List <ProductClass> product;
    private Context context;
    private boolean success = false;
    ConnectionDB DB;

    public ProductAdapter(Context context, List <ProductClass> product) {
        this.product = product;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from((context));
        View v = inflater.inflate(R.layout.product, null);
        return new ProductAdapter.ProductViewHolder(v);
    }

      @Override
      public void onBindViewHolder(@NonNull final ProductAdapter.ProductViewHolder holder, int position) {
          ProductClass productClass = product.get(position);
          holder.tvName.setText(productClass.getName());

          class Data extends AsyncTask <String, String, JSONArray> {
              JSONArray array = new JSONArray();
              JSONObject obj=null;
              JSONObject obj1 = null;
              String name="";
              @Override
              protected JSONArray doInBackground(String[] strings) {
                  try {
                      DB = new ConnectionDB();
                      DB.connection();
                      Statement st = (Statement) DB.connection().createStatement();
                      ResultSetImpl rs = (ResultSetImpl) st.executeQuery("select productimage from  Products where ActiveYn='Yes' and DeleteYn='No'");
                      ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
                      while (rs.next()) {
                          obj = new JSONObject();
                          name = "http://136.233.82.211/pub/media/catalog/product/" + rs.getString("productimage");
                         // System.out.println(name);
                          obj.put("name", name);
                          array.put(obj);
                          obj1 = new JSONObject();
                          obj1.put("images", array);
                      }
                  } catch (Exception e) {
                      e.printStackTrace();
                  }
                  return array;
              }

              @Override
              protected void onPostExecute(JSONArray array) {
                  super.onPostExecute(array);
                 // System.out.println(array);
              //    Glide.with(context).load(array).into(holder.ivProduct);
              }
          }
          Data t = new Data();
          t.execute();
      }
    @Override
    public int getItemCount() {
        return product.size();
    }
    public class ProductViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProduct;
        TextView tvName;
        EditText tvRate;
        Button btnCard;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            ivProduct = itemView.findViewById(R.id.ivProduct);
            tvName = itemView.findViewById(R.id.tvName);
            //  tvRate=itemView.findViewById(R.id.tvRate);

        }
    }

}
