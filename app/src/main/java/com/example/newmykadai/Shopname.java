package com.example.newmykadai;

public class Shopname {
    private  String title;
    private String image;
    private String address;
    private String city;
    private String zip;
    private String country;
    public Shopname(String title, String image, String address,String city,String zip,String country) {
        this.title = title;
        this.image = image;
        this.address = address;
        this.city=city;
        this.zip=zip;
        this.country=country;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
