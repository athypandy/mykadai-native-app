package com.example.newmykadai;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;


public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ShopViewHolder> {
    private List <Shopname> shopnameList;
    ItemClick activity;
    public interface ItemClick{
        void onItemClick(int index);
    }

    public ShopAdapter(Context context, List <Shopname> shopnameList) {
        activity=(ItemClick) context;
        this.shopnameList = shopnameList;
    }

    @NonNull
    @Override
    public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from((Context) activity);
        View v=inflater.inflate(R.layout.layout_row,null);
        return new ShopViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopViewHolder holder, int position) {
         Shopname shopname=shopnameList.get(position);
        Glide.with((Context) activity).load(shopname.getImage()).into(holder.ivPic);
         holder.tvResult.setText(shopname.getTitle());
         holder.tvAddress.setText(shopname.getAddress());
         holder.tvCity.setText(shopname.getCity());
         holder.tvZip.setText(shopname.getZip());
         holder.tvCountry.setText(shopname.getCountry());
    }

    @Override
    public int getItemCount() {
        return shopnameList.size();
    }
    class ShopViewHolder extends RecyclerView.ViewHolder{
       TextView tvResult,tvAddress,tvCity,tvZip,tvCountry;
       ImageView ivPic;
        public ShopViewHolder(@NonNull View itemView) {
            super(itemView);
            tvResult=itemView.findViewById(R.id.tvResult);
            ivPic=itemView.findViewById(R.id.ivPic);
            tvAddress=itemView.findViewById(R.id.tvAddress);
            tvCity=itemView.findViewById(R.id.tvCity);
            tvZip=itemView.findViewById(R.id.tvZip);
            tvCountry=itemView.findViewById(R.id.tvCountry);

           itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   activity.onItemClick(shopnameList.indexOf((Shopname)v.getTag()));
               }
           });
        }
    }
}
