package com.example.newmykadai;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.mysql.jdbc.ResultSetImpl;
import com.mysql.jdbc.ResultSetMetaData;
import com.mysql.jdbc.StatementImpl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class Grocery extends AppCompatActivity implements ShopAdapter.ItemClick {
List <Shopname> shopnameList;
RecyclerView recyclerView;
private boolean success=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery);
        recyclerView = findViewById(R.id.recyclerProduct);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        shopnameList=new ArrayList <Shopname>();
               ConnectMySql connect=new ConnectMySql();
               connect.execute();
    }



    private class ConnectMySql extends AsyncTask <String, Void, String> {
        String res = "";
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress=ProgressDialog.show(Grocery.this,"Synchronising","Grocery Loading! Please Wait...",true);

        }

        @Override
        protected String doInBackground(String... params) {

            try {
                ConnectionDB DB=new ConnectionDB();
                DB.connection();
                if(DB.connection()==null){
                   success=false;
                }else {
                    StatementImpl st = (StatementImpl) DB.connection().createStatement();
                    ResultSetImpl rs = (ResultSetImpl) st.executeQuery(" select * from shops where root_Category_id='3' and Activeyn='Yes' and Deleteyn='No'");
                    ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
                    if(rs!=null) {
                        while (rs.next()) {
                            try{
                                shopnameList.add(new Shopname(rs.getString("shopname"),rs.getString("ShopLogo"),rs.getString("Street2"),rs.getString("city"),rs.getString("Zipcode"),rs.getString("Country")));
                            }catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        res = "Found";
                        success=true;
                    }else{
                        res="No Data Found";
                        success=false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Writer writer=new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                res = writer.toString();
                success=false;
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            Toast.makeText(Grocery.this,res+"",Toast.LENGTH_SHORT).show();
            if(success==false){

            }else {
                try{
                    ShopAdapter adapter = new ShopAdapter(Grocery.this,shopnameList);
                    recyclerView.setAdapter(adapter);
                }catch (Exception ex){

                }
            }

        }
    }
    @Override
    public void onItemClick(int index) {
        Intent intent=new Intent(Grocery.this,ProductView.class);
        startActivity(intent);
    }


}